var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000,
    bodyParser = require('body-parser'),
    http = require('http'),
    cors = require('cors'),
    cookieParser = require('cookie-parser'),
    async = require('async'),
    hpp = require('hpp'),
    csurf = require('csurf'),
    response = require('./utils/response');

var oauthModel = require('./config/oauth2token');
var oAuth2Server = require('node-oauth2-server');

app.oauth = oAuth2Server({
    model: oauthModel,
    grants: ['password'],
    debug: true
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(hpp());
app.use(cookieParser());
app.use(csurf({
    cookie: {
        // here you can configure your cookie. Default values are ok, but I decided to be more explicit
        // http://expressjs.com/en/4x/api.html#req.cookies
        key: '_csrf',
        path: '/',
        httpOnly: false, // if you want you can use true here
        secure: false, // if you are using HTTPS I suggest true here
        signed: false, // I don't know if csurf supports signed cookies, so I used false
        // not mandatory, but if you want you can use sameSite: 'strict'
        // sameSite: 'strict', // https://www.owaspsafar.org/index.php/SameSite
        maxAge: 24 * 60 * 60 * 1000 // 24 hours
    }
}));
app.use((req, res, next) => {
    const csrfTokenToSendToFrontEnd = req.csrfToken();
    console.log('csrfTokenToSendToFrontEnd: ', csrfTokenToSendToFrontEnd);
    // this cookie must be XSRF-TOKEN, because already defined as default in Angular.
    res.cookie('XSRF-TOKEN', csrfTokenToSendToFrontEnd);
    next();
});

var users = require('./routes/users')(app);
var oauth = require('./routes/oauth')(app);

function parallel(middlewares) {
    return function (req, res, next) {
        async.each(middlewares, function (mw, cb) {
            mw(req, res, cb);
        }, next);
    };
}

app.use(parallel([
    users,
    oauth
]));

app.disable('x-powered-by');
app.use(function (req, res, next) {
    res.locals._csrf = req.csrfToken();
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-CSRF-Token, Origin, X-Requested-With, Content-Type, Accept");
    res.header('X-XSS-Protection', '1; mode=block');
    res.header('X-Frame-Options', 'deny');
    res.header('X-Content-Type-Options', 'nosniff');
    // next(createError(404));
    response.routeNotFound(res);
});

app.use(function (err, req, res, next) {
    if (err.code === 'EBADCSRFTOKEN') response.forbidden(err.message, res);
    if (err.name === 'OAuth2Error') {
        response.unauthorized(err.code, err.message, err.error, res);
    } else {
        // render the error page
        response.error(err, res);
    }
});

app.use(cors({
    origin: true,
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
}));

// app.use(express.json());
// app.use(express.urlencoded({ extended: true }));


http.createServer(app.listen(port));
console.log('RESTful API server started on: ' + port);
