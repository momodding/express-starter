const model = require('../models/index');
var response = require('../utils/response');

exports.checkToken = async function (req, res, next) {
    try {
        const users = await model.users.findOne({ attributes: ['name', 'email'] },{
            where: {
                id: req.user.id
            }
        });
        if (users) {
            response.success(users, '', "User found!", res);
        } else {
            response.notFound(users, '', "User not found!", res);
        }
    } catch (error) {
        response.error(error.messages, res);
    }
};

