'use strict';

const model = require('../models/index');
const { validationResult } = require('express-validator');
var response = require('../utils/response');

exports.get = async function (req, res, next) {
    try {
        const users = await model.users.findAll({});
        if (users.length !== 0) {
            response.success(users, '', "User found!", res);
        } else {
            response.notFound(users, '', "User not found!", res);
        }
    } catch (error) {
        response.error(error.messages, res);
    }
};

exports.getOne = async function (req, res, next) {
    try {
        const usersId = req.params.id;
        const users = await model.users.findOne({where: {
            id: usersId
        }});
        if (users) {
            response.success(users, '', "User found!", res);
        } else {
            response.notFound(users, '', "User not found!", res);
        }
    } catch (error) {
        response.error(error.messages, res);
    }
};

exports.post = async function (req, res, next) {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            // return res.status(422).json({ errors: errors.array() });
            response.unprocessableEntity(errors.array(), res);
        }
        const {
            name,
            email,
            gender,
            phoneNumber
        } = req.body;
        const users = await model.users.create({
            name,
            email,
            gender,
            phone_number: phoneNumber
        });
        if (users) {
            response.create(users, '', "User created!", res);
        }
    } catch (error) {
        response.badRequest(error.messages, res);
    }
};

exports.patch = async function (req, res, next) {
    try {
        const usersId = req.params.id;
        const {
            name,
            email,
            gender,
            phoneNumber
        } = req.body;
        const users = await model.users.update({
            name,
            email,
            gender,
            phone_number: phoneNumber
        }, {
            where: {
                id: usersId
            }
        });
        if (users) {
            response.success(users, '', "User updated!", res);
        }
    } catch (error) {
        response.badRequest(error.messages, res);
    }
};

exports.delete = async function (req, res, next) {
    try {
        const usersId = req.params.id;
        const users = await model.users.destroy({
            where: {
                id: usersId
            }
        });
        if (users) {
            response.success(users, '', "User deleted!", res);
        }
    } catch (error) {
        response.badRequest(error.messages, res);
    }
};