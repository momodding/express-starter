'use strict';
module.exports = (sequelize, DataTypes) => {
  const AccessToken = sequelize.define('AccessToken', {
    accessToken: DataTypes.STRING,
    clientId: DataTypes.STRING,
    userId: {
      type: DataTypes.INTEGER
    },
    expires: DataTypes.DATE
  }, {});
  AccessToken.associate = function(models) {
    // associations can be defined here
  };
  return AccessToken;
};