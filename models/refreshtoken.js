'use strict';
module.exports = (sequelize, DataTypes) => {
  const RefreshToken = sequelize.define('RefreshToken', {
    refreshToken: DataTypes.STRING,
    clientId: DataTypes.STRING,
    userId: DataTypes.INTEGER,
    expires: DataTypes.DATE
  }, {});
  RefreshToken.associate = function(models) {
    // associations can be defined here
  };
  return RefreshToken;
};