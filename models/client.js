'use strict';
module.exports = (sequelize, DataTypes) => {
  const client = sequelize.define('client', {
    clientId: {
      type: DataTypes.STRING,
      unique: true
    },
    clientSecret: DataTypes.STRING,
    clientType: {
      type: DataTypes.ENUM,
      values: ['public', 'confidential', 'web_application', 'native_application'],
      defaultValue: 'public'
    },
    userId: DataTypes.INTEGER,
    redirectUri: DataTypes.STRING
  }, {});
  client.associate = function(models) {
    // associations can be defined here
  };
  return client;
};