'use strict';

module.exports = function (app) {
    var oauth = require('../controller/oauth');

    app.route('/api/get_token')
        .post(app.oauth.grant());

    app.route('/api/check_token')
        .post(app.oauth.authorise(), oauth.checkToken);
};