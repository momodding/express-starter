'use strict';
const { check } = require('express-validator');

module.exports = function (app) {
    var person = require('../controller/users');

    app.route('/users')
        .get(person.get);

    app.route('/user/:id')
        .get(person.getOne);

    app.route('/user')
        .post([
            // username must be an email
            check('email').isEmail()
        ], person.post);

    app.route('/user/:id')
        .patch(person.patch);

    app.route('/user/:id')
        .delete(person.delete);
};