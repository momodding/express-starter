'use strict';

exports.success = function (data, info, message, res) {
    var response = {
        'status': {
            'code': 200,
            'debug': info
        },
        'message': message,
        'data': data
    };
    res.status(200)
    res.json(response);
    res.end();
};

exports.create = function (data, info, message, res) {
    var response = {
        'status': {
            'code': 201,
            'debug': info
        },
        'message': message,
        'data': data
    };
    res.status(201)
    res.json(response);
    res.end();
};

exports.unauthorized = function (status, info, message, res) {
    var response = {
        'status': {
            'code': status,
            'debug': info
        },
        'message': message,
        'data': null
    };
    res.status(status)
    res.json(response);
    res.end();
};

exports.notFound = function (data, info, message, res) {
    var response = {
        'status': {
            'code': 404,
            'debug': info
        },
        'message': message,
        'data': data
    };
    res.status(404)
    res.json(response);
    res.end();
};

exports.forbidden = function (info, res) {
    var response = {
        'status': {
            'code': 403,
            'debug': info
        },
        'message': "Your request can't be process",
        'data': null
    };
    res.status(403)
    res.json(response);
    res.end();
};

exports.routeNotFound = function (res) {
    var response = {
        'status': {
            'code': 404,
            'debug': ''
        },
        'message': 'Route not found!',
        'data': null
    };
    res.status(404)
    res.json(response);
    res.end();
};

exports.badRequest = function (info, res) {
    var response = {
        'status': {
            'code': 400,
            'debug': info.toString()
        },
        'message': 'Please check your input field!',
        'data': null
    };
    res.status(400)
    res.json(response);
    res.end();
};

exports.error = function (info, res) {
    var response = {
        'status': {
            'code': 500,
            'debug': info.toString()
        },
        'message': 'Something messed up in our side. Please try again later!',
        'data': null
    };
    res.status(500)
    res.json(response);
    res.end();
};

exports.unprocessableEntity = function (info, res) {
    var response = {
        'status': {
            'code': 422,
            'debug': info
        },
        'message': 'Please check your input again!',
        'data': null
    };
    res.status(422)
    res.json(response);
    res.end();
};
