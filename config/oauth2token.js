const model = require('../models/index');

module.exports = {
    getClient: getClient,
    saveAccessToken: saveAccessToken,
    getUser: getUser,
    grantTypeAllowed: grantTypeAllowed,
    getAccessToken: getAccessToken
}

function getClient(clientID, clientSecret, callback) {
    // console.log('getClient');
    callback(false, {
        clientID,
        clientSecret,
        grants: null,
        redirectUris: null
    });
}

function grantTypeAllowed(clientID, grantType, callback) {
    // console.log('grantTypeAllowed');
    callback(false, true);
}

function getUser(username, password, callback) {
    // console.log('getUser');

    model.users.findOne({
        where: {
            name: username,
            email: password
        }
    }).then(function (user) {
        callback(false, user ? user.id : null);
    }).catch(function (err) {
        console.log('err', err);
    });
}

function saveAccessToken(accessToken, clientID, expires, user, callback) {
    // console.log('saveAccessToken');
    model.AccessToken.findOne({
        where: {
            userId:user
        }
    }).then(function (token) {
        if (token) {
            model.AccessToken.update({
                accessToken: accessToken,
                clientId: clientID,
                expires: expires
            }, {where:{
                userId: user
            }}).then(function () {
                callback(false);
            }).catch(function (err) {
                console.log('err', err);
            });
        } else {
            model.AccessToken.create({
                accessToken: accessToken,
                userId: user,
                expires: expires
            }).then(function (accesstoken) {
                callback(null);
            });

        }
        return;
        callback(null);
    });
}

function getAccessToken(bearerToken, callback) {
    // console.log(bearerToken);
    model.AccessToken.findOne({where:{
        accessToken: bearerToken
    }
    }).then(function (token) {
        callback(null, token);
    }).catch(function (err) {
        console.log('err', err);
    });
}