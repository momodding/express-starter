'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      clientId: {
        type: Sequelize.STRING,
        unique: true
      },
      clientSecret: {
        type: Sequelize.STRING
      },
      clientType: {
        type: Sequelize.ENUM,
        values: ['public', 'confidential', 'web_application', 'native_application'],
        defaultValue: 'public'
      },
      userId: {
        type: Sequelize.INTEGER
      },
      redirectUri: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('clients');
  }
};